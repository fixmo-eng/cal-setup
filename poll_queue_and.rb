!#/usr/bin/env ruby
#require_relative 'test_cl_arg.rb'
require_relative 'android_device_selector.rb'

home = `echo $HOME`.gsub("\n", '')
queue_file = "#{home}/.queue_and"
temp_file = "#{home}/.queue_and_temp"

while true
	dir = "Temp value so that dir persists"
	#Check is build is queued
	if File.size?(queue_file) != nil
		File.open(queue_file,'r') do |f2|
			dir = f2.readline
		end
		directory = "#{home}/#{dir}"
		
		#Run automation scripts on build
		android_device_selector(directory)

        #Remove build from queue
		File.open(temp_file,'w') do |f2|
			File.readlines(queue_file).each_with_index do |line, index|
				if index == 0
					next
				end
				line.strip
				f2.puts line
			end
		end 
		File.delete(queue_file)
		File.rename(temp_file,queue_file)
	end
	sleep 10
end