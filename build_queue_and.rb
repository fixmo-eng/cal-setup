!#/usr/bin/env ruby

def build_queue_and(build_num)
    home = `echo $HOME`.gsub("\n", '')
    queue_file = "#{home}/.queue_and"
    temp_file = "#{home}/.queue_and_temp"
    dir = "qa.calabash.safezone.android.#{build_num}"
    dir_check = 0

    #Make a directory for the APK
    apk = "master-release.apk"
    slave_apk = "slave-release.apk"
    anddir = "#{home}/#{dir}"
#    `ssh-add ~/.ssh/fixmo-idreeskhan`
    puts `ssh-add ~/.ssh/id_rsa`
    puts `rm -rf #{anddir}`
    puts `mkdir #{anddir}`
    
    #Fetch feature files
    puts `git clone git@bitbucket.org:fixmo-eng/qa.calabash.safezone.android.git #{anddir}`
    Dir.chdir("#{anddir}")
    
    #Get the APK from Jenkins
    puts `wget http://build01.fixmo.corp:8080/job/SafeZone%20Android%20Trunk/#{build_num}/artifact/SafeGuardAndroidClientLauncher/#{apk}`
    puts `wget http://build01.fixmo.corp:8080/job/SafeZone%20Android%20Trunk/#{build_num}/artifact/SafeGuardAndroidClientLauncher/#{slave_apk}`
    puts `calabash-android resign #{apk}`
    puts `calabash-android resign #{slave_apk}`
    puts `adb uninstall com.fixmo.slave`
    puts `adb uninstall com.fixmo.safeguard.android.workspace`
    puts `adb uninstall com.fixmo.safeguard.android`
    puts `adb install -r #{slave_apk}`
    Dir.chdir("#{home}")
    `touch #{queue_file}`

    File.open(queue_file,'a') do |f2|
        f2.puts dir
    end
end
