!#/usr/bin/env ruby
require 'thread'
require 'Semaphore'
require 'net/smtp'

def android_device_selector(dir)
    start_time = Time.now
    dir=dir.gsub("\n",'')
    #puts "#{dir}test"
    Dir.chdir("#{dir}/")

    #for debugging purposes and for purposes that may or may not be useful
    /Gets a list of feature files that need to be run and a list of devices to run on/
    device_list = `adb devices`
    device_list = device_list.scan(/\n\w+\tdevice/)
    device_list = device_list.map {|s| s.gsub("\tdevice",'')}
    device_list = device_list.map {|s| s.gsub("\n",'')}
    device_list = device_list.shuffle
    #device_list.reverse!
    num_devices = device_list.count
    
    #Get a list of feature files in the features directory
    feature_list = `ls features/*.feature`
    feature_list = feature_list.scan(/.*\n/)
    feature_list = feature_list.map {|s| s.gsub("\n",'')}
    feature_list = feature_list - ["features/Enroll.feature", "features/Decommission.feature", "features/Docs.feature", "features/Shareplace.feature"]
    feature_list = feature_list.shuffle
    num_features = feature_list.count

    puts "There are #{num_devices} devices connected to run #{num_features} feature files"

    enrolled_devices = Array.new
    device_ports = Array.new
    thread_arr = Array.new
    base_port = 34800
    puts "Using #{base_port} as the base communication port"

    device_queue_lock = Mutex.new
    enroll_lock = Mutex.new
    device_semaphore = Semaphore.new(num_devices)
    count = 0

    #Start a thread for each feature file to be run
    for i in 0 ... feature_list.count
        thread_arr << Thread.new {
            #feature_queue_lock = Mutex.new
            device = "Temp value so that the variable exists outside of lock.synchronize scope"
            feature = "Some persistent value"
            port = base_port

            device_semaphore.P #decrement semaphore
        
            #Wait for a device to be available
            device_queue_lock.synchronize { #lock
                device = device_list.shift
                feature = feature_list.shift
                port = base_port+count
                puts "In thread for #{device} with count=#{count}"
                count = count + 1
                #ENV["ADB_DEVICE_ARG"] = device
            } #unlock

        
            #tmp = `adb -s #{device} shell pm list packages`.include? "#{$PackageName}"
            
            #Enroll device if it is not enrolled
            tmp = enrolled_devices.include? "#{device}"
            if tmp == false
                enroll_lock.synchronize { #lock
                puts "#{device} running features/Enroll.feature on port #{port}"
                Dir.chdir("#{dir}/")
                #puts `ADB_DEVICE_ARG=#{device} TEST_SERVER_PORT=#{port} calabash-android run master-release.apk features/Enroll.feature --format html --out Enroll_#{device}.htm`
                puts `export ADB_DEVICE_ARG=#{device};export TEST_SERVER_PORT=#{port}; calabash-android run master-release.apk features/Enroll.feature --format html --out Enroll_#{device}.htm`       
                enrolled_devices << device
                device_ports << port
                puts "Enrolled devices: " + enrolled_devices.inspect
                }#unlock
            end
        
            feature_stripped = feature.gsub("features/",'').gsub(".feature",'')

            #Run scenarios in feature file
            index = enrolled_devices.index("#{device}").to_i
            port = device_ports[index]
            puts "#{device} running #{feature} on port #{port}"
            Dir.chdir(dir)
            puts `export ADB_DEVICE_ARG=#{device};export TEST_SERVER_PORT=#{port}; calabash-android run master-release.apk --format html --out #{feature_stripped}_#{device}.htm #{feature}`
        
            #Make device availabe
            device_queue_lock.synchronize { #lock
                device_list << device
            } #unlock
        
            device_semaphore.V #increment semaphore
        
            puts "List of free devices: " + device_list.inspect
        }
    end
    Thread.abort_on_exception = true

    #Wait for all features to be tested
    thread_arr.each do |thr|
        thr.join
    end

    #Decommission all devices
    enrolled_devices.each do |device|
        #feature_name = `cat features/Decommission.feature`.scan(/Feature:\s\w+\n/)[0].gsub("Feature: ", '').gsub("\n", '').gsub(" ", "_")
        #`echo #{device} > .#{feature_name}`
        index = enrolled_devices.index("#{device}").to_i
        port = device_ports[index]
        puts "#{device} running features/Decommission.feature on port #{port}"
        Dir.chdir("#{dir}/")
        puts `export ADB_DEVICE_ARG=#{device};export TEST_SERVER_PORT=#{port}; calabash-android run master-release.apk --format html --out Decommission_#{device}.htm features/Decommission.feature`
    end

    current_day=start_time.strftime("%B %-d, %Y")
    end_time=Time.now

    Dir.chdir("#{dir}/")

    #Move reports to new directory
    puts `mkdir reports`
    puts `mv *.htm reports`
    puts `mv *.png reports`
    # puts `mkdir reports/Enr_and_Decomm`
    # puts `mv reports/Enroll* reports/Enr_and_Decomm/`
    # puts `mv reports/Decommission* reports/Enr_and_Decomm/`
    puts `zip -vr reports.zip reports/`

    #Send zipped reports to an email address
    build_number = dir.split('.')[-1]

    sender='clientside_automator@local.host'
    recipients=['joon.kwon@fixmo.com', 'david.thomson@fixmo.com', 'eric.lazam@fixmo.com', 'jeanne.li@fixmo.com', 'Wasiur.Rahman@fixmo.com', 'Vincent.Hong@fixmo.com']
    relay_host='10.150.99.11' #relay01.fixmo.corp
    
    report_file_name='reports.zip'
    report_dir="#{dir}/#{report_file_name}"
    report_content = File.read(report_dir)
    encodedcontent = [report_content].pack("m") #base64 encoding

    marker = "AUNIQUEMARKER"

    body =<<END_OF_MESSAGE
Automatically generated email with Calabash-Android results attached. 

Build Number: #{build_number}
Started at: #{start_time} Ended at: #{end_time}.
END_OF_MESSAGE

    part1 =<<END_OF_MESSAGE
From: clientside_automator <clientside_automator@qa.fixmo.com>
Content-Type: multipart/mixed; 
    boundary="#{marker}"
To: QA Rocks! <fantastic.qa@fixmo.com>
Subject: Calabash-Android Automation Results #{current_day}, Build #: #{build_number}
Mime-Version: 1.0

--#{marker}
END_OF_MESSAGE

    part2 =<<END_OF_MESSAGE
Content-Transfer-Encoding: 7bit
Content-Type: text/plain

#{body}

--#{marker}
END_OF_MESSAGE

    part3 =<<END_OF_MESSAGE
Content-Disposition: attachment;
    filename=#{report_file_name}
Content-Type: application/zip; 
    name="#{report_file_name}"
Content-Transfer-Encoding: base64

#{encodedcontent}

--#{marker}--
END_OF_MESSAGE

    msgstr = part1 + part2 + part3

    begin
        Net::SMTP.start(relay_host,25) do |smtp|
            smtp.send_message msgstr, sender, recipients
        end
    rescue Exception => e
        puts "Exception occurred!"
        puts e.message
        puts e.backtrace.join("\n")
    end

    # File.open("script3.scpt",'w') do |f2|
    #     f2.puts "tell application \"Mail\"" 
    #     f2.puts "make new outgoing message with properties {visible:true,subject:\"Calabash-Android Automation Results #{current_day}\",content:\"Automatically generated email with Calabash-iOS results attached. Started at: #{start_time} Ended at: #{end_time}.\"}" 
    #     f2.puts "tell result" 
    #     f2.puts "make new to recipient at end of to recipients with properties {address:\"joon.kwon@fixmo.com\"}" 
    #     f2.puts "make new to recipient at end of to recipients with properties {address:\"david.thomson@fixmo.com\"}" 
    #     f2.puts "make new to recipient at end of to recipients with properties {address:\"eric.lazam@fixmo.com\"}" 
    #     f2.puts "make new attachment with properties {file name:\"#{dir}/reports.zip\"}" 
    #     f2.puts "send" 
    #     f2.puts "end tell" 
    #     f2.puts "end tell"
    # end 
    # `/usr/bin/osascript script3.scpt`
end
