#! /bin/bash

if [  $# -ne 1 ]; then
    echo
    echo "Usage; $0 <commit_hash_number>"
    echo
    exit 1
fi

export DEVICE_TARGET='e1e5d1b541bc8187cae6dce28326291b724c8c5f'
export DEVICE_ENDPOINT="http://10.150.102.117:37265"
export BUNDLE_ID="com.fixmo.SecureContainer.SafeZone.Workspace"

IPA_NAME="safezoneapp.ipa"
REPO_NAME="szn.safezone.ios"

COMMIT_HASH_SHORT=${1:0:6}
DIR=$HOME/$REPO_NAME.$COMMIT_HASH_SHORT
SAFEZONEAPP_DIR=$DIR/SafeZoneApp

ssh-add $HOME/.ssh/id_rsa
rm -rf $DIR

cd $HOME
git clone git@bitbucket.org:fixmo-eng/szn.safezone.ios.git $DIR
cd $DIR

git pull
git reset --hard $1

cd $SAFEZONEAPP_DIR
git clone git@bitbucket.org:fixmo-eng/qa.calabash.safezone.ios.git tmp
mv tmp/* ./
rm -rf tmp

calabash-ios setup
sleep 10s

open ../SafeZone.xcworkspace
sleep 240s
kill -9 $(ps -ef | grep '/Applications/Xcode.app/Contents/MacOS/Xcode' | grep -v grep | head -n 1 |  awk '{ print $2}')

xcodebuild -workspace ../SafeZone.xcworkspace -scheme SafeZoneApp-cal -configuration Release -destination "platform=iOS,id=${DEVICE_TARGET}" archive

ARCHIVE_BASE="${HOME}/Library/Developer/Xcode/Archives/$(date +%Y-%m-%d)" 
NEW_ARCHIVE=$(ls -t $ARCHIVE_BASE | head -n 1) 

xcodebuild -exportArchive -archivePath "${ARCHIVE_BASE}/${NEW_ARCHIVE}" -exportFormat IPA -exportPath $IPA_NAME -exportProvisioningProfile "iOS Team Provisioning Profile: *"

ideviceinstaller -u $DEVICE_TARGET -U $BUNDLE_ID
ideviceinstaller -u $DEVICE_TARGET -i $IPA_NAME

echo "$SAFEZONEAPP_DIR/$IPA_NAME has been produced and installed onto $DEVICE_TARGET device."
echo ""
echo "Please Enroll SafeZone first"
echo "If you want to continue to run test, press y:"
read CONTINUE
if [ $CONTINUE == "y" ] || [ $CONTINUE == "Y" ] ; then
    cucumber features/Browser.feature features/Calendar.feature features/Camera.feature features/Contacts.feature features/Email.feature --format html --out ios_${COMMIT_HASH_SHORT}.htm
else
    echo "Bye"
    exit 1
fi