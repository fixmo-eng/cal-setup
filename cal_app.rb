!#/usr/bin/env ruby
require 'sinatra'
require 'thread'
require 'thin'

set :server, 'thin'

Dir[File.join(File.dirname(__FILE__), 'build_queue_and.rb')].sort.each do |file|
  require file
end

Dir[File.join(File.dirname(__FILE__), 'build_queue.rb')].sort.each do |file|
  require file
end

get '/' do
    'This is a web service configured for the purposes of automating Calabash scripts on completed successful Jenkins builds'
end

post '/test' do
    "I got #{params[:test]}"
end

post '/build_android' do
    puts "I got #{params[:build_number]}.\n"
    t1 = Thread.new{
	sleep 120
        build_queue_and(params[:build_number])
    }
end

post '/build_ios' do
    puts "I got #{params[:commit_hash]}.\n"
    t2 = Thread.new{
        build_queue(params[:commit_hash])
    }
end
