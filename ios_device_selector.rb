!#/usr/bin/env ruby
require 'thread'
#require 'Semaphore'
require 'net/smtp'

def ios_device_selector(dir)
    start_time = Time.now
    home = ENV["HOME"]
    #home = "#{home}/calabash"
    dir=dir.gsub("\n",'')
    szbase=dir
    safezoneapp_dir="#{dir}/SafeZoneApp"
    safezoneapp_ipa="#{safezoneapp_dir}/safezoneapp.ipa"
    Dir.chdir(safezoneapp_dir)

    #List of IPs of connected devices
    device_list=["10.150.102.117"]
    device_id_list=["e1e5d1b541bc8187cae6dce28326291b724c8c5f"]
    device_list.map! {|ele| ele = "http://" + ele + ":37265"}

    puts `ideviceinstaller -u #{device_id_list[0]} -U com.fixmo.SecureContainer.SafeZone.Workspace 2>&1`
    puts `ideviceinstaller -u #{device_id_list[0]} -i #{safezoneapp_ipa}`

    feature_list = `ls features/*.feature`
    feature_list = feature_list.scan(/.*\n/)
    feature_list = feature_list.map {|s| s.gsub("\n",'')}
    feature_list = feature_list - ["features/Enroll.feature", "features/Decommission.feature", 'features/Shareplace.feature', 'features/Docs.feature']

    feature_order =''
    for feature in feature_list
        feature_order = feature_order+' '+feature
    end

    #puts `DEVICE_TARGET=#{device_id_list[0]} DEVICE_ENDPOINT=#{device_list[0]} BUNDLE_ID=com.fixmo.SecureContainer.SafeZone.Workspace cucumber features/Enroll.feature #{feature_order} features/Decommission.feature --format html --out #{dir}.html`
    puts `echo "device target: #{device_id_list[0]}"`
    puts `export DEVICE_TARGET=#{device_id_list[0]} ; export DEVICE_ENDPOINT=#{device_list[0]} ; export BUNDLE_ID=com.fixmo.SecureContainer.SafeZone.Workspace ; cucumber features/Enroll.feature#{feature_order} features/Decommission.feature --format html --out #{safezoneapp_dir}/calabash-ios-test.htm`

    #Get values for reporting
    current_day=start_time.strftime("%B %-d, %Y")
    end_time=Time.new
    
    #Move all html reports into a new directory
    puts `rm -rf #{safezoneapp_dir}/reports 2>&1`
    puts `rm #{safezoneapp_dir}/reports.zip 2>&1`
    puts `mkdir #{safezoneapp_dir}/reports 2>&1`
    puts `mv #{safezoneapp_dir}/*.htm #{safezoneapp_dir}/reports 2>&1`
    puts `mv #{safezoneapp_dir}/*.png #{safezoneapp_dir}/reports 2>&1`
    puts `cd #{safezoneapp_dir} ;zip -vr reports.zip reports/ 2>&1`

        #Send zipped reports to an email address
    build_number = dir.split('.')[-1]

    sender='clientside_automator@local.host'
    recipients=['joon.kwon@fixmo.com', 'david.thomson@fixmo.com', 'eric.lazam@fixmo.com', 'jeanne.li@fixmo.com', 'Wasiur.Rahman@fixmo.com', 'Vincent.Hong@fixmo.com']
    relay_host='10.150.99.11' #relay01.fixmo.corp
    
    report_file_name='reports.zip'
    report_dir="#{safezoneapp_dir}/#{report_file_name}"
    report_content = File.read(report_dir)
    encodedcontent = [report_content].pack("m") #base64 encoding

    marker = "AUNIQUEMARKER"

    body =<<END_OF_MESSAGE
Automatically generated email with Calabash-iOS results attached. 

Build Number: #{build_number}
Started at: #{start_time} Ended at: #{end_time}.
END_OF_MESSAGE

    part1 =<<END_OF_MESSAGE
From: clientside_automator <clientside_automator@qa.fixmo.com>
Content-Type: multipart/mixed; 
    boundary="#{marker}"
To: QA Rocks! <fantastic.qa@fixmo.com>
Subject: Calabash-iOS Automation Results #{current_day}, Build #: #{build_number}
Mime-Version: 1.0

--#{marker}
END_OF_MESSAGE

    part2 =<<END_OF_MESSAGE
Content-Transfer-Encoding: 7bit
Content-Type: text/plain

#{body}

--#{marker}
END_OF_MESSAGE

    part3 =<<END_OF_MESSAGE
Content-Disposition: attachment;
    filename=#{report_file_name}
Content-Type: application/zip; 
    name="#{report_file_name}"
Content-Transfer-Encoding: base64

#{encodedcontent}

--#{marker}--
END_OF_MESSAGE

    msgstr = part1 + part2 + part3

    begin
        Net::SMTP.start(relay_host,25) do |smtp|
            smtp.send_message msgstr, sender, recipients
        end
    rescue Exception => e
        puts "Exception occurred!"
        puts e.message
        puts e.backtrace.join("\n")
    end
	
    #puts `rm -rf #{home}/#{dir} 2>&1`
end
