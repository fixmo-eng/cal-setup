!#/usr/bin/env ruby

def build_queue(commit_hash)
    device_id_list=['e1e5d1b541bc8187cae6dce28326291b724c8c5f']
    home = `echo $HOME`.gsub("\n", '')
    queue_file = "#{home}/.queue"
    temp_file = "#{home}/.queue_temp"
    szbase = "#{home}/szn.safezone.ios"
    commit_hash_concat = commit_hash[0...6]
    dir = "szn.safezone.ios.#{commit_hash_concat}"

    #Change the clean copy of SZ to commit
    `ssh-add #{home}/.ssh/id_rsa`
    `rm -rf #{home}/#{dir}*`
    Dir.chdir(home)
    `git clone git@bitbucket.org:fixmo-eng/szn.safezone.ios.git #{home}/#{dir}`
    Dir.chdir("#{home}/#{dir}")
    `git pull`
    `git reset --hard #{commit_hash}`

    Dir.chdir("#{home}/#{dir}/SafeZoneApp")
    
    #Add feature files and set up calabash within the project copy
    `git clone git@bitbucket.org:fixmo-eng/qa.calabash.safezone.ios.git tmp`
    Dir.chdir("#{home}/#{dir}/SafeZoneApp/tmp")
    `mv * ..`
    Dir.chdir("#{home}/#{dir}/SafeZoneApp")
    puts `pwd`
    `rm -rf tmp`
    sleep 3
    puts `calabash-ios setup 2>&1`
    sleep 10
    puts `open ../SafeZone.xcworkspace`
    sleep 240
    puts `kill -9 $(ps -ef | grep '/Applications/Xcode.app/Contents/MacOS/Xcode' | grep -v grep | head -n 1 |  awk '{ print $2}')`
    sleep 10
    puts `kill -9 $(ps -ef | grep '/Applications/Xcode.app/Contents/MacOS/Xcode' | grep -v grep | head -n 1 |  awk '{ print $2}')`
    puts `xcodebuild -workspace ../SafeZone.xcworkspace -scheme SafeZoneApp-cal -configuration Release -destination "platform=iOS,id=#{device_id_list[0]}" archive 2>&1`
    puts `ARCHIVE_BASE="${HOME}/Library/Developer/Xcode/Archives/$(date +%Y-%m-%d)" ; NEW_ARCHIVE=$(ls -t $ARCHIVE_BASE | head -n 1) ; xcodebuild -exportArchive -archivePath "${ARCHIVE_BASE}/${NEW_ARCHIVE}" -exportFormat IPA -exportPath ./safezoneapp.ipa -exportProvisioningProfile "iOS Team Provisioning Profile: *" 2>&1`

    `touch #{queue_file}`

    File.open(queue_file,'a') do |f2|
        f2.puts dir
    end
end

